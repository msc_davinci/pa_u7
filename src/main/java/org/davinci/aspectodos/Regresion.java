package org.davinci.aspectodos;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Regresion {
    public static void main(String[] args) {
        double sx=0.0, sy=0.0, sxy=0.0, sx2=0.0, a, b, estimacion;
        int n, year;
        Scanner teclado = new Scanner(System.in);
        System.out.println("Proyeccion de ventas(años): ");
        year = teclado.nextInt();
        Queue<Venta> venta = new LinkedList<Venta>();
        venta.add(new Venta(2, 1));
        venta.add(new Venta(3, 3));
        venta.add(new Venta(5, 7));
        venta.add(new Venta(7, 11));
        venta.add(new Venta(9, 15));
        venta.add(new Venta(10, 17));
        n = venta.size();
        System.out.println("Año \t Ventas" );
        while (!venta.isEmpty()) {
            Venta p = venta.remove();
            sx = sx + p.getYear();
            sy = sy + p.getMontoVenta();
            sx2 = sx2 + p.getYear()*p.getYear();
            sxy = sxy + p.getYear()* p.getMontoVenta();
            System.out.println(p.toString());
        }

        a = (sx*sy-n*sxy)/(sx*sx-n*sx2);
        b = (sx*sxy-sx2*sy)/(sx*sx-n*sx2);
        estimacion = a*year + b;
        System.out.println("sx = " + sx + "; sy = " + sy + "; sxy = " + sxy );
        System.out.println("a = " + a + "; b = " + b );
        System.out.println("Estimación("+year+") = " + (estimacion) );
    }
}
