package org.davinci.aspectodos;

public class Venta {
    private int year;
    private double montoVenta;
    public Venta(int year, double montoVenta) {
        this.year = year;
        this.montoVenta = montoVenta;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public double getMontoVenta() {
        return montoVenta;
    }
    public void setMontoVenta(double montoVenta) {
        this.montoVenta = montoVenta;
    }

    @Override
    public String toString() {
        return year + "\t " + montoVenta;
    }
}
